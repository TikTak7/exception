package calculator;

public class Subtraction extends Do{
	private static String name = "Вычитание";  

    public Subtraction(double a, double b) {
        this.a = a;
        this.b = b;
    }
    @Override
    public double getArea() {
    	return a - b;
    }    
    @Override
    public String getName() {
        return name;
    }    
    public double geta() {
    	return a;
    }
    public void seta(double a) {
        this.a = a;
    }
    public double getb() {
        return b;
    }
    public void setb(double b) {
        this.b = b;
    }

}
